package com.dgx.softexpertdemo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dgx.softexpertdemo.R;
import com.dgx.softexpertdemo.api.model.CarModel;
import com.dgx.softexpertdemo.databinding.RcCarBinding;

public class CarPagedAdapter extends PagedListAdapter<CarModel, CarPagedAdapter.ItemViewHolder> {

    private static final DiffUtil.ItemCallback<CarModel> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<CarModel>() {
                @Override
                public boolean areItemsTheSame(CarModel oldItem, CarModel newItem) {
                    return oldItem.getId().equals(newItem.getId());
                }


                @SuppressLint("DiffUtilEquals")
                @Override
                public boolean areContentsTheSame(CarModel oldItem, @NonNull CarModel newItem) {
                    return oldItem.equals(newItem);
                }
            };
    private final Context context;


    public CarPagedAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.context = context;
    }

    @NonNull
    @Override
    public CarPagedAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RcCarBinding itemView = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rc_car, parent, false);
        return new CarPagedAdapter.ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CarPagedAdapter.ItemViewHolder holder, int position) {
        CarModel item = getItem(position);
        if (item != null) {
            holder.setViewModel(item);
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CarPagedAdapter.ItemViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CarPagedAdapter.ItemViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {

        private RcCarBinding binding;

        ItemViewHolder(RcCarBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind();
            }
        }

        void setViewModel(final CarModel model) {
            if (binding != null) {

                binding.setCar(model);

                if (model.getImageUrl() != null && !model.getImageUrl().trim().isEmpty())
                    Glide.with(context).asBitmap().load(model.getImageUrl()).diskCacheStrategy(DiskCacheStrategy.DATA).placeholder(R.color.gray).into(binding.img);
                else
                    binding.img.setImageResource(R.color.gray);


            }
        }
    }


}
