package com.dgx.softexpertdemo.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.dgx.softexpertdemo.R;
import com.dgx.softexpertdemo.databinding.RvLoadingBinding;

public class LoadingAdapter extends RecyclerView.Adapter<LoadingAdapter.ViewHolder> {

    private final int size;


    public LoadingAdapter(int size) {
        this.size = size;
    }

    @NonNull
    @Override
    public LoadingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RvLoadingBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_loading, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull LoadingAdapter.ViewHolder holder, int position) {
        if (position != RecyclerView.NO_POSITION)
            holder.setViewModel();
    }

    @Override
    public int getItemCount() {
        return size;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull LoadingAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull LoadingAdapter.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private RvLoadingBinding binding;

        ViewHolder(RvLoadingBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind();
            }
        }

        void setViewModel() {
            if (binding != null) {
                binding.loader.startShimmerAnimation();
            }
        }

    }

}




