package com.dgx.softexpertdemo;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dgx.softexpertdemo.adapter.CarPagedAdapter;
import com.dgx.softexpertdemo.adapter.LoadingAdapter;
import com.dgx.softexpertdemo.api.OnLoadDataListener;
import com.dgx.softexpertdemo.databinding.ActivityMainBinding;
import com.dgx.softexpertdemo.pagaing.CarViewModel;
import com.dgx.softexpertdemo.utils.NetworkState;
import com.dgx.softexpertdemo.utils.Utils;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private ActivityMainBinding binding;
    private CarViewModel itemViewModel;
    private CarPagedAdapter carsAdapter;
    private final OnLoadDataListener updateData = new OnLoadDataListener() {
        @Override
        public void onDataLoaded(int count) {

            switch (count) {
                case NetworkState.UN_AUTHORISE:
                    Toast.makeText(getApplicationContext(), "No Auth", Toast.LENGTH_SHORT).show();
                    break;
                case NetworkState.LOADING:

                    loadingDate();
                    break;
                case NetworkState.NO_NET:
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    break;
                case 0:
                    Toast.makeText(getApplicationContext(), "No Data", Toast.LENGTH_SHORT).show();
                    break;
                case NetworkState.LOADED:
                default:
                    binding.rvCars.setAdapter(carsAdapter);

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.swipeRefreshLayout.setOnRefreshListener(this);
        loadingDate();
        init();
    }

    private void init() {
        carsAdapter = new CarPagedAdapter(this);
        binding.rvCars.setLayoutManager(new LinearLayoutManager(this));
        binding.rvCars.setAdapter(carsAdapter);
        itemViewModel = ViewModelProviders.of(this).get(CarViewModel.class);
        itemViewModel.setUpdate(updateData);
        itemViewModel.itemPagedList.observe(this, carsAdapter::submitList);
    }

    @Override
    public void onRefresh() {

        if (Utils.isNetworkAvailable(this)) {
            loadingDate();
            if (itemViewModel != null)
                itemViewModel.refresh();
            else
                init();
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        binding.swipeRefreshLayout.setRefreshing(false);

    }

    private void loadingDate() {
        LoadingAdapter loadingAdapter = new LoadingAdapter(5);
        binding.rvCars.setLayoutManager(new LinearLayoutManager(this));
        binding.rvCars.setAdapter(loadingAdapter);
    }
}