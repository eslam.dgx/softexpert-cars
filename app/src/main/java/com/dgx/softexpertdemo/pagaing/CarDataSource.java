package com.dgx.softexpertdemo.pagaing;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.dgx.softexpertdemo.api.ApiClient;
import com.dgx.softexpertdemo.api.ApiInterface;
import com.dgx.softexpertdemo.api.OnLoadDataListener;
import com.dgx.softexpertdemo.api.model.CarModel;
import com.dgx.softexpertdemo.api.model.ListCarsResponse;
import com.dgx.softexpertdemo.utils.NetworkState;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarDataSource extends PageKeyedDataSource<Integer, CarModel> {


    private OnLoadDataListener update;

    public void setListener(OnLoadDataListener update) {
        this.update = update;
    }


    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, CarModel> callback) {


        if (update != null)
            update.onDataLoaded(NetworkState.LOADING);


        ApiInterface mApiService = ApiClient.getInterfaceService();
        Call<ListCarsResponse> call = mApiService.listCars(1);
        call.enqueue(new Callback<ListCarsResponse>() {
            @Override
            public void onResponse(@NonNull Call<ListCarsResponse> call, @NonNull Response<ListCarsResponse> response) {

                if (response.body() != null) {
                    if (update != null)
                        update.onDataLoaded(response.body().getData().size());
                    callback.onResult(response.body().getData(), null, 2);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ListCarsResponse> call, @NonNull Throwable t) {

                if (update != null)
                    update.onDataLoaded(NetworkState.NO_NET);
            }
        });
    }


    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, CarModel> callback) {


        ApiInterface mApiService = ApiClient.getInterfaceService();
        Call<ListCarsResponse> call = mApiService.listCars(params.key);
        call.enqueue(new Callback<ListCarsResponse>() {
            @Override
            public void onResponse(@NonNull Call<ListCarsResponse> call, @NonNull Response<ListCarsResponse> response) {

                Integer adjacentKey = (params.key > 1) ? params.key - 1 : null;
                if (response.body() != null) {
                    if (update != null)
                        update.onDataLoaded(response.body().getData().size());
                    callback.onResult(response.body().getData(), adjacentKey);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ListCarsResponse> call, @NonNull Throwable t) {

                if (update != null)
                    update.onDataLoaded(NetworkState.NO_NET);
            }
        });

    }


    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, CarModel> callback) {

        ApiInterface mApiService = ApiClient.getInterfaceService();
        Call<ListCarsResponse> call = mApiService.listCars(params.key);
        call.enqueue(new Callback<ListCarsResponse>() {
            @Override
            public void onResponse(@NonNull Call<ListCarsResponse> call, @NonNull Response<ListCarsResponse> response) {

                if (response.body() != null) {
                    if (update != null)
                        update.onDataLoaded(response.body().getData().size());
                    callback.onResult(response.body().getData(), params.key + 1);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ListCarsResponse> call, @NonNull Throwable t) {

                if (update != null)
                    update.onDataLoaded(NetworkState.NO_NET);
            }
        });
    }


}
