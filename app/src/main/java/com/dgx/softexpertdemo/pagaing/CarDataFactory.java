package com.dgx.softexpertdemo.pagaing;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.dgx.softexpertdemo.api.OnLoadDataListener;
import com.dgx.softexpertdemo.api.model.CarModel;


public class CarDataFactory extends DataSource.Factory {

    private final MutableLiveData<PageKeyedDataSource<Integer, CarModel>> itemLiveDataSource = new MutableLiveData<>();
    private CarDataSource itemDataSource;
    private OnLoadDataListener update;

    @NonNull
    @Override
    public DataSource<Integer, CarModel> create() {
        itemDataSource = new CarDataSource();
        itemLiveDataSource.postValue(itemDataSource);
        itemDataSource.setListener(update);
        return itemDataSource;
    }


    MutableLiveData<PageKeyedDataSource<Integer, CarModel>> getItemLiveDataSource() {
        return itemLiveDataSource;
    }

    public void setUpdate(OnLoadDataListener update) {
        this.update = update;
    }


    public void refresh() {
        if (itemDataSource != null)
            itemDataSource.invalidate();
    }
}

