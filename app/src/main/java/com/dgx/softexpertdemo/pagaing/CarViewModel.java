package com.dgx.softexpertdemo.pagaing;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.dgx.softexpertdemo.api.OnLoadDataListener;
import com.dgx.softexpertdemo.api.model.CarModel;

import java.util.concurrent.Executor;


public class CarViewModel extends ViewModel implements OnLoadDataListener {

    private final LiveData<PageKeyedDataSource<Integer, CarModel>> liveDataSource;
    private final CarDataFactory itemDataSourceFactory;
    public LiveData<PagedList<CarModel>> itemPagedList;
    private OnLoadDataListener update;

    //constructor
    public CarViewModel() {

        MainThreadExecutor executor = new MainThreadExecutor();

        itemDataSourceFactory = new CarDataFactory();

        //getting the live data source from data source factory
        liveDataSource = itemDataSourceFactory.getItemLiveDataSource();

        //Getting PagedList config
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setPageSize(5).build();


        itemPagedList = (new LivePagedListBuilder(itemDataSourceFactory, pagedListConfig)).setFetchExecutor(executor).build();
        itemDataSourceFactory.setUpdate(this);
    }

    public void setUpdate(OnLoadDataListener update) {
        this.update = update;
    }

    @Override
    public void onDataLoaded(int count) {
        if (update != null)
            update.onDataLoaded(count);
    }


    public void refresh() {
        itemDataSourceFactory.refresh();
    }

    private static class MainThreadExecutor implements Executor {

        private final Handler mHandler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(@NonNull Runnable command) {
            mHandler.post(command);
        }

    }
}
