package com.dgx.softexpertdemo.api.model;

import com.google.gson.annotations.SerializedName;

public class CarModel {

    @SerializedName("id")
    private String id;

    @SerializedName("brand")
    private String brand;

    @SerializedName("constractionYear")
    private String constractionYear;

    @SerializedName("isUsed")
    private String isUsed;


    @SerializedName("imageUrl")
    private String imageUrl;

    public String getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getConstractionYear() {
        return constractionYear;
    }

    public String isUsed() {
        return "uses = " + isUsed;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
