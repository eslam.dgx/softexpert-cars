package com.dgx.softexpertdemo.api;

import com.dgx.softexpertdemo.api.model.ListCarsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiInterface {


    @GET("cars")
    Call<ListCarsResponse> listCars(@Query("page") int pageNum);


}
