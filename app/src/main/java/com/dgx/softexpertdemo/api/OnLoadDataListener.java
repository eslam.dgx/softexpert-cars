package com.dgx.softexpertdemo.api;

public interface OnLoadDataListener {

    void onDataLoaded(int count);
}
