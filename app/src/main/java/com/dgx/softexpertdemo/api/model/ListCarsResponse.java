package com.dgx.softexpertdemo.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ListCarsResponse {

    @SerializedName("status")
    private int status;

    @SerializedName("data")
    private List<CarModel> data;

    public int getStatus() {
        return status;
    }

    public List<CarModel> getData() {
        return data;
    }
}
